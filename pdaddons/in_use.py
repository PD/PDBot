from os import unlink, path

class InvalidState(Exception):
    pass

class in_use:
    def __init__(self, state=False, name=".in_use"):
        if type(state) is not bool:
            raise InvalidState("Invalid state "+repr(state))
            raise SystemExit
        if state:
            with open(name, "w+") as f:
                pass
        self.name=name

    def check(self):
        return path.exists(self.name)

    def state(self, state):
        if type(state) is not bool:
            raise InvalidState("Invalid State "+repr(state))
            raise SystemExit
        if state:
            with open(self.name, "w+"):
                pass
        if not state:
            try:
                unlink(self.name)
            except:
                pass
        return
