import os, json, asyncio, threading, sys, wikipedia
from random import randint
import discord, random
from discord.ext import commands
from discord.utils import get
from hata.ext.commands import setup_ext_commands
from hata import Embed, Color, ROLES, enter_executor
from hata.ext.extension_loader import EXTENSION_LOADER

from pdaddons.hata.dpy import MClient
from pdaddons.in_use import in_use

# to load from .env
from dotenv import load_dotenv
load_dotenv()

client = commands.Bot(command_prefix=os.environ.get('PREFIX'), case_insensitive=True, description='PDBot - v 0.9.0',
                        status=discord.Status.idle, activity=discord.Game(name='Compiling'))

client.remove_command('help')

pdbot = MClient(os.environ.get("TOKEN"))
setup_ext_commands(pdbot, os.environ.get('PREFIX'))

global active_projects
active_projects = []

@client.listen()
async def on_ready():
    print(f"{'='*40}\n{' '*20}Online!\n{'='*40}")
    await client.change_presence(status=discord.Status.online, activity=discord.Game(name='~$ ./PDBot'))

@client.command()
async def help(ctx, *, arguments=None):
    if not arguments:
        cmds = {}
        with open('./cmds/dcmds.json', 'w') as f:
            for command in client.commands:
                desc = command.description.split('||')
                if command.name != 'help':
                    cmds[command.name] = {"desc": desc[0], 'syntax': desc[-1], 'required_roles': [], 'required_perms': []}
            json.dump(cmds, f, indent=2)
            f.close()

        cmd_list = []
        for y in os.listdir('./cmds/'):
            with open('./cmds/'+y, 'r') as f:
                x = json.load(f)
            for i in x:
                if i.lower() != 'help':
                    cmd_list.append(i)
        p = commands.Paginator(prefix='```fix')
        for i in cmd_list:
            p.add_line(f'>> {i}')
        for page in p.pages:
            embed = discord.Embed(title='[List of commands]', color=discord.Color.from_rgb(0, 191, 255),
                            description=page)
            embed.set_footer(text=f'Requested by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
            await ctx.channel.send(embed=embed)

EXTENSION_LOADER.add_default_variables(
    client=client,
    pdbot=pdbot,
    get=get,
    discord=discord,
    random=random,
    randint=randint,
    os=os,
    json=json,
    asyncio=asyncio,
    threading=threading,
    sys=sys,
    wikipedia=wikipedia,
    Embed=Embed,
    Color=Color,
    ROLES=ROLES,
    enter_executor=enter_executor,
    active_projects=active_projects,
    commands=commands,
    in_use=in_use
)

for base_path, _, files in os.walk('./cogs/py'):
    for file in files:
        if file.endswith('.py'):
            EXTENSION_LOADER.load_extension(os.path.join(base_path, file).replace('\\', '/').replace('/', '.')[:-3][2:])
            print(f'Loaded {file}')


pdbot.start()
client.run(os.environ.get("TOKEN"))
pdbot.stop()
