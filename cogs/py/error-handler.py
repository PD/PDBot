@client.listen()
async def on_command_error(ctx, error):
    if isinstance(error, commands.MissingRequiredArgument) or isinstance(error, commands.CommandError):
        message = await ctx.channel.fetch_message(ctx.message.id)
        user_cmd = message.content.split()[0].split(os.environ.get('PREFIX'))[1]
        json_files = os.listdir("cmds")
        cmds = []
        for cmd in json_files:
            with open(f'cmds/{cmd}') as f:
                data = json.load(f)
            for i in data:
                cmds.append(i)
        if user_cmd not in cmds:
            embed = discord.Embed(title='Unknown Command! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                    description=f'Command not found! Do `{os.environ.get("PREFIX")}help` for a list of commands!')
            embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
            await ctx.channel.send(embed=embed)
        if user_cmd in cmds:
            with open(f'cmds/{cmd}') as f:
                json_output = json.load(f)[user_cmd]
            desc = json_output['desc']
            syntax = json_output['syntax']
            embed = discord.Embed(title='Syntax Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                    description=f'Description: **{desc}**\nUsage: ```css\n{os.environ.get("PREFIX")}{user_cmd} {syntax}```')
            embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
            await ctx.channel.send(embed=embed)
    elif isinstance(error, commands.MissingAnyRole):
        embed = discord.Embed(title='Warning ❗', color=discord.Color.from_rgb(178, 34, 34),
                        description='You are not allowed to use this command!')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
    elif isinstance(error, commands.BadArgument):
        embed = discord.Embed(title='Argument Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                        description='The arguments for this command was not entered correctly!')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
    elif isinstance(error, commands.MissingRole):
        embed = discord.Embed(title='Warning ❗', color=discord.Color.from_rgb(178, 34, 34),
                        description='You are not allowed to use this command!')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
    else:
        embed = discord.Embed(title='Error ❗', color=discord.Color.from_rgb(178, 34, 34),
                        description=str(error))
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
