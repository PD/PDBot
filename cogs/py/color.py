from colour import Color

@client.command(description='Returns the hex and rgba of the color||<color>')
async def color(ctx, *, color: str):
    try:
        color = color.lower()
        clr = Color(color)
        rgb = tuple(int(c*255) for c in clr.rgb)
        embed = discord.Embed(title=color, color=discord.Color.from_rgb(*rgb))
        embed.add_field(name="HEX:", value=clr.hex)
        embed.add_field(name="RGB:", value=rgb)
        embed.set_footer(text=f'Requested by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.send(embed=embed)
    except Exception as e:
        embed = discord.Embed(title='Color not found ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                description=f'Failed due to: {e}')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.send(embed=embed)
