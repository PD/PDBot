@client.command(description='Adds a dev to your project||<project-name> <username>')
async def ad(ctx, projectname: str, user: discord.User):
    projectname = projectname.lower()
    user = ctx.guild.get_member(user.id)
    category = get(ctx.guild.categories, name=projectname)

    if get(ctx.author.roles, name=f'{projectname} Founder'):
        if user != ctx.message.author:
            if not user:
                embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                        description='That user does not exist!')
                embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
                await ctx.channel.send(embed=embed)
            else:
                if not user.bot:
                    if not category:
                        embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                                description='That project does not exist!')
                        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
                        await ctx.channel.send(embed=embed)
                    else:
                        if not get(user.roles, name=f'{projectname} Dev'):
                            await user.add_roles(get(ctx.guild.roles, name=f'{projectname} Dev'))
                            embed = discord.Embed(color=discord.Color.from_rgb(0, 255, 0),
                                                    description=f'User **{user}** added to **{projectname}**!')
                            embed.set_footer(text=f'Dev added by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
                            await ctx.channel.send(embed=embed)
                        else:
                            embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                                    description=f'**{user}** is already a part of **{projectname}**!')
                            embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
                            await ctx.channel.send(embed=embed)
                else:
                    embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                            description=f'**{user}** is a bot!')
                    embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
                    await ctx.channel.send(embed=embed)
        else:
            embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                    description='You cannot add yourself as Dev!')
            embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
            await ctx.channel.send(embed=embed)
    else:
        embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                description='You must be the founder of the project to add devs!')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
