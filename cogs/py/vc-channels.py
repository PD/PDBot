@client.listen()
async def on_voice_state_update(member, before, after):
    vc_role = member.guild.get_role(706860693521039390)
    try:
        if member.voice.channel.id == 668288521219473428 or member.voice.channel.id == 700929196544753674:
            if not member.bot:
                if not vc_role in member.roles:
                    await member.add_roles(vc_role)
    except:
        if not member.bot:
            if vc_role in member.roles:
                await member.remove_roles(vc_role)
