@client.command(description='Searches the wikipedia for results||<topic>')
async def wiki(ctx, *, topic: str):
    topic = topic.lower()
    try:
        page = wikipedia.page(topic)
        summery = wikipedia.summary(topic, sentences=4)
        url = page.url
        image = random.choice(page.images)
        embed = discord.Embed(title=f'{topic} | {url}', description=summery, color=discord.Color.from_rgb(0, 191, 255))
        embed.set_thumbnail(url=image)
        await ctx.send(embed=embed)
    except:
        embed = discord.Embed(title='Unable to find page ⚠️', color=discord.Color.from_rgb(255, 0, 0),
                                description=f'Failed to find the page relating to {topic}!')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.send(embed=embed)
