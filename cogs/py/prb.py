@client.command(description='Removes a bot from your project||<project-name> <bot-name>')
async def prb(ctx, projectname: str, bot: discord.Member):
    projectname = projectname.lower()
    if get(ctx.guild.roles, name=f'{projectname} Founder'):
        if get(ctx.author.roles, name=f"{projectname} Founder"):
            if bot:
                if bot.bot:
                    channels = get(ctx.guild.categories, name=projectname).channels
                    for channel in channels:
                        await channel.set_permissions(bot, overwrite=None)
                    embed = discord.Embed(color=discord.Color.from_rgb(0, 255, 0), description=f'{bot.mention} removed from **{projectname}**!')
                    embed.set_footer(text=f'Bot removed from project by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
                    await ctx.channel.send(embed=embed)
                else:
                    embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                            description='You may only remove bots from your project!')
                    embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
                    await ctx.channel.send(embed=embed)
            else:
                embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                        description='That bot does not exist!')
                embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
                await ctx.channel.send(embed=embed)
        else:
            embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                    description='You must be the founder of the project to add channels!')
            embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
            await ctx.channel.send(embed=embed)
    else:
        embed = discord.Embed(title='Error! ⚠️', color=discord.Color.from_rgb(255, 255, 51),
                                description='That project does not exist!')
        embed.set_footer(text=f'Attempted by: {ctx.message.author}', icon_url=ctx.author.avatar_url)
        await ctx.channel.send(embed=embed)
